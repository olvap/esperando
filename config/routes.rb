Rails.application.routes.draw do
  get 'calendar/index'

  root to: 'calendar#index'

  resources :bets, only: [:new, :create, :edit, :update]
end
