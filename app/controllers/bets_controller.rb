class BetsController < ApplicationController
   http_basic_authenticate_with name: ENV['AUTHENTICATION_NAME'], password: ENV['AUTHENTICATION_PASSWORD'], only: [:edit, :update]

  def new
    @bet = Bet.new(when: params[:when])
  end

  def create
    @bet = Bet.new(bet_params)

    respond_to do |format|
      if @bet.save
        format.html { redirect_to root_path, notice: 'Tu apuesta fue registrada!' }
        format.json { render :show, status: :created, location: @bet }
      else
        format.html { render :new }
        format.json { render json: @bet.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @bet = Bet.find(params[:id])
  end

  def update
    @bet = Bet.find(params[:id])

    respond_to do |format|
      if @bet.update(bet_params)
        format.html { redirect_to root_path, notice: 'Tu apuesta fue actualizada!' }
        format.json { render :show, status: :created, location: @bet }
      else
        format.html { render :new }
        format.json { render json: @bet.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def bet_params
    params.require(:bet).permit(:name, :when)
  end
end
